# Svelte + Flask Template

A template for future Svlete + Flask project where the routing is already set up.

Run the following for development:
 - `python server.py` to start the Flask server.
 - `cd client; npm i; npm run build` to build and reload the Svelte frontend when it's changed.

 This demo also uses Tailwind to style the application.

Check the branches for finished projects.
