import { cleanup, render } from '@testing-library/svelte';

import Error from '../src/util/Error.svelte';

describe('Error', () => {
  const message = 'error';

  afterEach(cleanup);

  test('should render', () => {
    const { getByText } = render(Error, { message });

    expect(getByText('error'));
  });
});
