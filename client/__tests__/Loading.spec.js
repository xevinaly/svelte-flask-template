import { cleanup, render } from '@testing-library/svelte';

import Loading from '../src/util/Loading.svelte';

describe('Loading', () => {
  afterEach(cleanup);

  test('should render', () => {
    const { getByRole } = render(Loading);

    expect(getByRole('presentation'));
  });
});
