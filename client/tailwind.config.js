const isProduction = !process.env.ROLLUP_WATCH;
module.exports = {
  theme: {
  },
  variants: {
    backgroundColor: ['DEFAULT', 'hover'],
    extend: {
      textColor: ['active']
    }
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio')
  ],
  purge: {
    content: [
      "./src/**/*.svelte",
    ],
    defaultExtractor: content => {
      const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || []
      const broadMatchesWithoutTrailingSlash = broadMatches.map(match => _.trimEnd(match, '\\'))
      const matches = broadMatches
        .concat(broadMatchesWithoutTrailingSlash)
      return matches
    },
    enabled: isProduction
  },
};
