import axios from 'axios';

const axiosAPI = axios.create();

const apiRequest = (method, url, request) => {
    const headers = {
        authorization: ''
    };

    return axiosAPI({
        method,
        url,
        data: request,
        headers
      }).then(res => {
        return Promise.resolve(res.data);
      })
      .catch(err => {
        return Promise.reject(err);
      });
};

const get = (url, request) => apiRequest('get', url, request);

const put = (url, request) => apiRequest('put', url, request);

const API ={
    get,
    put
};

export default API;
