const plugin = require('tailwindcss/plugin');

const BASE_STYLES = {
  '*': {
    'scrollbar-color': 'initial',
    'scrollbar-width': 'initial'
  }
};

const SCROLLBAR_SIZE_BASE = {
  '--scrollbar-track': 'initial',
  '--scrollbar-thumb': 'initial',
  '--scrollbar-border': 'initial',
  'scrollbar-color': 'var(--scrollbar-thumb) var(--scrollbar-track) var(--scrollbar-border)',

  'overflow': 'overlay',

  '&::-webkit-scrollbar-track': {
    'background-color': 'var(--scrollbar-track)'
  },

  '&::-webkit-scrollbar-thumb': {
    'background-color': 'var(--scrollbar-thumb)',
    'border-color': 'var(--scrollbar-border)'
  }
};

const SCROLLBAR_SIZE_UTILITIES = {
  '.scrollbar': {
    ...SCROLLBAR_SIZE_BASE,
    'scrollbar-width': 'auto',

    '&::-webkit-scrollbar': {
      width: '16px',
      height: '16px'
    }
  },

  '.scrollbar-thin': {
    ...SCROLLBAR_SIZE_BASE,
    'scrollbar-width': 'thin',

    '&::-webkit-scrollbar': {
      width: '8px',
      height: '8px'
    }
  },

  '.scrollbar-none': {
    ...SCROLLBAR_SIZE_BASE,
    'scrollbar-width': 'none',

    '&::-webkit-scrollbar': {
      width: '0px',
      height: '0px'
    }
  },

  '.scrollbar-hover-only': {
    ...SCROLLBAR_SIZE_BASE,
    'scrollbar-width': 'none',

    '&::-webkit-scrollbar': {
      width: '0px',
      height: '0px'
    },

    '&:hover': {
      'scrollbar-width': 'thin'
    },

    '&:hover::-webkit-scrollbar': {
      width: '8px',
      height: '8px'
    }
  }
};

const generateScrollbarColorUtilities = (key, value) => ({
  [`.scrollbar-track-${key}`]: {
    '--scrollbar-track': value
  },

  [`.scrollbar-thumb-${key}`]: {
    '--scrollbar-thumb': value
  },

  [`.scrollbar-border-${key}`]: {
    '--scrollbar-border': value
  },

  [`.hover\\:scrollbar-thumb-${key}`]: {
    '&::-webkit-scrollbar-thumb:hover': {
      '--scrollbar-thumb': value
    }
  }
});

const generateScrollbarRadiusUtilities = (key, value) => {
  let className = '.scrollbar-rounded';

  if (key !== 'DEFAULT') {
    className += `-${key}`;
  }

  return {
    [className]: {
      '&::-webkit-scrollbar-thumb': {
        'border-radius': value
      }
    }
  };
};

const generateScrollbarBorderWidthUtilities = (key, value) => {
  let className = '.scrollbar-border';

  if (key !== 'DEFAULT') {
    className += `-${key}`;
  }

  return {
    [className]: {
      '&::-webkit-scrollbar-thumb': {
        'border-width': value,
        'border-style': 'solid'
      }
    }
  };
};

module.exports = plugin(function ({ e, addUtilities, theme, addBase, variants }) {
  const scrollbarVariants = variants('scrollbar', []);

  const generateAllScrollbarColorUtilities = (colors, prefix = '') => Object.entries(colors)
    .reduce((memo, [key, value]) => ({
      ...memo,
      ...(
        typeof value === 'object'
          ? generateAllScrollbarColorUtilities(value, `${e(key)}-`)
          : generateScrollbarColorUtilities(key === 'DEFAULT' ? prefix.replace(/-$/, '') : `${prefix}${e(key)}`, value)
      )
    }), {});

  const generateAllScrollbarRadiusUtilities = radii => Object.entries(radii)
    .reduce((memo, [key, value]) => ({
      ...memo,
      ...generateScrollbarRadiusUtilities(e(key), value)
    }), {});

  const generateAllScrollbarBorderWidthUtilities = width => Object.entries(width)
    .reduce((memo, [key, value]) => ({
      ...memo,
      ...generateScrollbarBorderWidthUtilities(e(key), value)
    }), {});

  const scrollbarBorderWidthUtilities = generateAllScrollbarBorderWidthUtilities(theme('borderWidth', {}));
  const scrollbarRadiusUtilities = generateAllScrollbarRadiusUtilities(theme('borderRadius', {}));
  const scrollbarColorUtilities = generateAllScrollbarColorUtilities(theme('colors', {}));

  addBase(BASE_STYLES);

  addUtilities({
    ...SCROLLBAR_SIZE_UTILITIES,
    ...scrollbarBorderWidthUtilities,
    ...scrollbarRadiusUtilities,
    ...scrollbarColorUtilities
  });
});
