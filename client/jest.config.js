const fs = require('fs');
const path = require('path');

const setupTestsFile = true;

module.exports = function () {

  const userSvelteConfig = getUserSvelteConfig();

  return {
    rootDir: './',
    transform: {
      '^.+\\.svelte$': [
        'svelte-jester',
        { 'preprocess': true },
      ],
      '^.+\\.js$': 'babel-jest',
      '\\.html$': 'svelte-mock/transform',
      '\\.svelte$': 'svelte-mock/transform'
    },
    moduleFileExtensions: [
      'js',
      'svelte'
    ],
    bail: false,
    verbose: true,
    silent: false,
    automock: false,
    setupFilesAfterEnv: [
      '<rootDir>/jest-setup.js'
    ],
    resolver: 'jest-svelte-resolver',
    transformIgnorePatterns: [
      'node_modules/(?!@ngrx|(?!deck.gl)|ng-dynamic)',
      'node_modules/(?!(svelte-awesome)/)'
    ]
  };
};

function getUserSvelteConfig() {
  const userSvelteConfigLoc = path.join(process.cwd(), 'svelte.config.js');
  if (fs.existsSync(userSvelteConfigLoc)) {
    return require(userSvelteConfigLoc);
  }

  return {};
}
